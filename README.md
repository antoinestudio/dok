# Dok

Documentation tool written in Python.

## Installation

### Git ignore

First, make sure to have a `.gitignore` file in your root folder so that you only upload the files you need to build your website, that means the processed/minimized files and not the raw files.

```
public/*
__pycache__/
doc.py
Pipfile.lock
content/*/**/*.*
!content/*/**/*.md
.git
.vscode
```

### Python modules

Install [Python 3](https://www.python.org/) and [pip](https://pypi.org/project/pip/) (the package installer for Python), and then the python modules needed to run this project.

`$ pip install os shutil markdown2 Pillow jinja2 beautifulsoup4 datetime rcssmin libsass lxml PyYAML feedgen`

### Public folder

Make sure to have a folder called `public/` and a `public/medias` and `public/assets` sub-folder in your project.

### Launch the python script

`python main.py`

## Article informations

```
title: Article title
last_update: 2020-01-01
tags: truc, troc
featured: True
featured_image: image_file.jpg
excerpt: This is the excerpt of the article
```

## Architecture

<pre>
content/
public/
-- medias/
templates/
</pre>

## Types

- draft
- page

## Content

<pre>
---
title: Post title
---
</pre>

[link](url)
[button](button:url)
[file](file:url)
![image description](imagepath)
![image description](large:imagepath)
![image description](small:imagepath)
